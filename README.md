Pre-reqs
=========

* Python 3.6
* Virtualenv
* SQLite

Installation
=============
* Clone the code
* Create `carbot/local_settings.py` and adjust the `TELEGRAM_TOKEN` and `SALE_TRACK_DOMAIN` variables
* `python -u manage.py migrate`
* Optionally, `python -u manage.py migrate createsuperuser`

Running the bot
===============

Simply run `python -u manage.py runbot` and ensure your token is set correctly.

(Development mode) Admin interface
===================================

* `python -u manage.py runserver`
* Open a browser at http://localhost:8000/admin/

