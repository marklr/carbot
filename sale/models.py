from django.db import models
from tagging.fields import TagField


class Sale(models.Model):
    ctime = models.DateTimeField(auto_now_add=True)
    orig_url = models.TextField()
    short_url = models.CharField(max_length=256)
    description = models.TextField()
    score = models.IntegerField(default=1)
    source = models.TextField(blank=True, null=True)
    tags = TagField()


class Vote(models.Model):
    ctime = models.DateTimeField(auto_now_add=True)
    sale = models.ForeignKey(Sale, related_name="votes")
    ip = models.GenericIPAddressField(blank=True, null=True)
    user_agent = models.TextField(null=True, blank=True)
