from django.conf import settings
from telegram.ext import Updater, CommandHandler
from django.core.management.base import BaseCommand
from django.db.models import F
from tagging.models import TaggedItem
from sale.models import Sale
from sale.util import drop_affiliate
from pprint import pformat
import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def start(bot, update):
    return help(bot, update)


def help(bot, update):
    update.message.reply_text("Hi! Please use /search <tag1> (tag2 tag3 tag4 ...) to find sales or /sale <link> <description> [#tag1, ...] ")


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def create_sale(url=None, descr="", tags=[], source="telegram"):
    sale, created = Sale.objects.get_or_create(orig_url=drop_affiliate(url, settings.AFFILIATE_TAGS))
    sale.tags += ",".join(tags)
    if not created:
        logger.info(f'Adding tags for sale {sale.pk}: {tags}')
        sale.score = F('score') + 1
        sale.save()

        return sale

    sale.description = descr
    sale.source = source
    sale.save()
    return sale


def add_sale(bot, update):
    url = None
    descr = []
    tags = []

    for token in update.message.text.lower().strip().split():
        if not token:
            continue

        if token.lower().strip() == "/sale":
            continue

        if token.startswith('#'):
            tmptags = set(list(filter(lambda z: z, [x.lower() for x in token.split('#')])))
            tags += list(tmptags)
            continue

        if token.startswith('http:') or token.startswith('https:'):
            url = token
            continue

        descr.append(token)

    if url and descr:
        sale = create_sale(url, " ".join(descr), tags)
        if sale:
            update.message.reply_text(f'Thank you! <i>{sale.pk}</i>', parse_mode='HTML')


def search(bot, update):
    logger.info(pformat(update))
    if not update.message.from_user:
        logger.warning("No from_user?")
        return

    msg = []
    total = 0
    for sale in TaggedItem.objects.get_by_model(Sale, update.message.text.lower().strip().split()[1:]).order_by('-score')[0:5]:
        total += 1
        url = f'https://{settings.SALE_TRACK_DOMAIN}/v/{sale.pk}/'
        msg.append(f'[{sale.score}]  <a href="{url}">{sale.description}</a>')

    if total:
        msg.insert(0, f'Found {total} matching sales.\r\n')
        bot.send_message(update.message.chat_id, "\r\n".join(msg), parse_mode='HTML',
                         disable_web_page_preview=True,
                         disable_notification=True)
    else:
        bot.send_message(update.message.chat_id, "Nothing found. Try more popular tags, like 'amazon' or 'prime'".join(msg),
                         parse_mode='HTML',
                         disable_web_page_preview=True,
                         disable_notification=True
                         )
    logger.info("\r\n".join(msg))


class Command(BaseCommand):
    # See https://core.telegram.org/bots/api

    help = 'Invokes the sale bot in the current terminal session'

    def handle(self, *args, **options):
        updater = Updater(settings.TELEGRAM_TOKEN)
        dp = updater.dispatcher

        # on different commands - answer in Telegram
        dp.add_handler(CommandHandler("start", start))
        dp.add_handler(CommandHandler("search", search))
        dp.add_handler(CommandHandler("sale", add_sale))
        dp.add_handler(CommandHandler("help", help))
        dp.add_error_handler(error)

        # on noncommand i.e message - echo the message on Telegram
        # dp.add_handler(MessageHandler(Filters.text, echo))
        logger.info("Starting loop, SIGINT to abort.")
        updater.start_polling()

        # Run the bot until you press Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        updater.idle()
