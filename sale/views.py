from django.db.models import F
from django.shortcuts import get_object_or_404
from django.views.generic.base import RedirectView
from ipware.ip import get_ip

from sale.models import Sale, Vote


class VoteCounterRedirectView(RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'sale'

    def get_redirect_url(self, *args, **kwargs):
        sale = get_object_or_404(Sale, pk=kwargs['pk'])
        sale.score = F('score') + 1
        sale.save()

        v = Vote(ip=get_ip(self.request),
                 sale=sale,
                 user_agent=self.request.META.get('HTTP_USER_AGENT', ""))
        v.save()

        return sale.orig_url
