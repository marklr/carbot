from urllib.parse import parse_qs, urlparse, urlunparse, urlencode


def drop_affiliate(url, tags):
    try:
        u = urlparse(url.lower().strip())
    except Exception:
        return url

    qs = parse_qs(u.query)
    for t in tags:
        qs.pop(t, None)

    u = u._replace(query=urlencode(qs, True))
    return urlunparse(u)
